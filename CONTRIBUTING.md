# Contributor guide

## How do I make a bug report?

- Describe the bug in short
- Include all necessary information about the bug:
  - Full error message (including stack trace if available)
  - Your platform and version (E.g: Node v20.14.0)
  - Screenshots if necessary
- Explain how we can reproduce this bug

## Styleguides

### Commit messages

We use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/#summary) for commit messages to simplify versioning.

## Workflow

1. If it's your first time setting up the project it's recommended to build all packages sequentially first:

    ```sh
    pnpm build:sequential
    ```

    You can start using the `build` or `watch` scripts after this

2. Make your changes and document them with `pnpm changeset`

3. Commit your changes and push them to the

4. Submit a merge/pull request
