# Wuwa Lib Utils

> Utilities for the wuwa library

## Usage

1. Install the dependency:

    ```sh
    npm install wuwa-lib-utils
    ```

2. Use it in your project:

    ```ts
    import { getPartialConveneHistoryPayloadFromLogFile } from 'wuwa-lib-utils'

    const partialPayload = getPartialConveneHistoryPayloadFromLogFile('/path/to/debug.log')
    ```
