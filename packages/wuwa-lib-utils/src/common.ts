/** Check if a value is an object. */
export const isExactObject = (value: unknown): value is Record<PropertyKey, unknown> =>
    typeof value === "object" && !Array.isArray(value) && value !== null;

type UnknownRecord = Record<PropertyKey, unknown>;

/** Recursively override nullish values in an object. */
export const applyDefaults = <T extends UnknownRecord = UnknownRecord>(defaults: T, overrides: T) => {
    const finalObject = structuredClone(defaults);

    for (const [key, value] of Object.entries(overrides)) {
        if (isExactObject(value)) {
            finalObject[key as keyof T] =
                key in defaults
                    ? (applyDefaults(defaults[key] as UnknownRecord, value) as T[keyof T])
                    : (value as T[keyof T]);
            continue;
        }

        finalObject[key as keyof T] = (value as T[keyof T]) ?? (finalObject[key] as T[keyof T]);
    }

    return finalObject;
};
