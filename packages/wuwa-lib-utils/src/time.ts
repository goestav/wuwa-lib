import { WuwaServerId } from "wuwa-api-types";

/**
 * Get the (time zone aware) date for a {@link WuwaServerId|wuthering waves server}.\
 * **NOTE:** if the `serverId` is not a valid {@link WuwaServerId} value then the timezone will be adjusted to UTC+0.
 */
export const getDateFromString = (timeString: string, serverId: WuwaServerId | string) => {
    const timeZoneOffset = getUTCTimeZoneOffsetForServer(serverId);
    const timeZoneOffsetString = `Z${timeZoneOffset}`;

    const date = new Date(timeString + timeZoneOffsetString);

    return date;
};

/**
 * Get the UTC time zone offset string for a {@link WuwaServerId|wuthering waves server}.\
 * **NOTE:** if the `serverId` is not a valid {@link WuwaServerId} value then the timezone will be adjusted to UTC+0.
 *
 * @example
 * getUTCTimeZoneOffsetForServer(WuwaServerId.Europe) // '+1'
 */
export const getUTCTimeZoneOffsetForServer = (serverId: WuwaServerId | string) => {
    switch (serverId) {
        case WuwaServerId.America:
            return "-5";
        case WuwaServerId.Europe:
            return "+1";
        case WuwaServerId.Asia:
        case WuwaServerId.HMT:
        case WuwaServerId.SEA:
            return "+8";
        default:
            return "";
    }
};
