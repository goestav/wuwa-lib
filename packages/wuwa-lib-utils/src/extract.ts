import { readFile } from "node:fs/promises";
import type { ConveneHistoryPayload } from "wuwa-api-types";

interface ExtractAPIParametersFromLogFileArrayBufferOptions {
    /**
     * The encoding for the {@link ArrayBuffer}.
     * @default "utf-8"
     */
    encoding?: BufferEncoding;
    /**
     * The encoding for the {@link TextDecoder}.
     * @default "utf-8"
     */
    textDecoderEncoding?: string;
}

interface ExtractAPIParametersFromLogFileBufferOptions {
    encoding?: BufferEncoding;
    textDecoderEncoding?: string;
}

type ExtractAPIParametersFromLogFileOptions =
    | ExtractAPIParametersFromLogFileBufferOptions
    | ExtractAPIParametersFromLogFileArrayBufferOptions;

/** Represents the URL search parameters for the convene history endpoint. */
export interface ConveneHistoryURLParameters {
    /** The server ID. */
    svr_id: string;
    /** The player ID. */
    player_id: string;
    /** The language that will be used for the results. */
    lang: string;
    gacha_id: string;
    gacha_type: string;
    /** The server type (presumably the environment like global or test server). */
    svr_area: string;
    record_id: string;
    /** The resources ID (can be mapped to `"cardPoolId"`). */
    resources_id: string;
}

/** The convene history query body partially without the `cardPoolType` key. */
type ConveneHistoryPartialPayload = Omit<ConveneHistoryPayload, "cardPoolType">;

interface OverloadInterface<T> {
    (input: string | Buffer, options?: ExtractAPIParametersFromLogFileBufferOptions): T;
    (input: string | ArrayBuffer, options?: ExtractAPIParametersFromLogFileArrayBufferOptions): T;
    (input: string | Buffer | ArrayBuffer, options?: ExtractAPIParametersFromLogFileOptions): T;
}

const getLogFileContent: OverloadInterface<Promise<string>> = async (
    input: string | Buffer | ArrayBuffer,
    options?: ExtractAPIParametersFromLogFileOptions,
) => {
    if (input instanceof ArrayBuffer) {
        const decoder = new TextDecoder(options?.textDecoderEncoding ?? "utf-8");

        return decoder.decode(input);
    }

    if (input instanceof Buffer) {
        return input.toString(options?.encoding ?? "utf-8");
    }

    return readFile(input, { encoding: options?.encoding ?? "utf-8" });
};

/** The regex that matches the convene history URL. */
export const conveneHistoryURLRegex =
    /(https?:\/\/aki-gm-resources-oversea\.aki-game\.net\/aki\/gacha\/index\.html#\/record\b[-a-zA-Z0-9()@:%_\+.~#?&//=]*)/m;

/** Extract all {@link URLSearchParams|URL parameters} from the convene history URL found in the debug log file. */
export const extractConveneHistoryEndpointParametersFromLogFile: OverloadInterface<
    Promise<ConveneHistoryURLParameters>
> = async (input: string | Buffer | ArrayBuffer, options?: ExtractAPIParametersFromLogFileOptions) => {
    const logFileContent = await getLogFileContent(input, options);

    const matches = logFileContent.matchAll(new RegExp(conveneHistoryURLRegex, "gm"));
    const match = [...matches].at(-1);

    if (!match?.[0])
        throw new Error(
            "No convene history API url found in the log file, please make sure to load your pull history in-game before attempting to load the debug file in",
        );

    const url = new URL(match[0]);
    const parameters = new URLSearchParams(url.hash.replace("#/record", ""));

    return Object.fromEntries(parameters.entries()) as unknown as ConveneHistoryURLParameters;
};

/** Get the partial convene history payload from the debug log file. */
export const getPartialConveneHistoryPayloadFromLogFile: OverloadInterface<
    Promise<ConveneHistoryPartialPayload>
> = async (input: string | Buffer | ArrayBuffer, options?: ExtractAPIParametersFromLogFileOptions) => {
    const conveneHistoryURLParameters = await extractConveneHistoryEndpointParametersFromLogFile(input, options);

    return {
        playerId: conveneHistoryURLParameters.player_id,
        cardPoolId: conveneHistoryURLParameters.resources_id,
        serverId: conveneHistoryURLParameters.svr_id,
        languageCode: conveneHistoryURLParameters.lang,
        recordId: conveneHistoryURLParameters.record_id,
    };
};
