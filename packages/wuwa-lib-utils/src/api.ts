import type { WuwaAPIServerErrorResponse, WuwaAPIResponse, WuwaAPISuccessResponse } from "wuwa-api-types";

/** Type-guard to check against a successful API request body. */
export const checkResponseSuccessBody = <T>(
    responseJson: WuwaAPIResponse<T>,
): responseJson is WuwaAPISuccessResponse<T> => {
    return responseJson.code === 0;
};

/** Type-guard to check against an error API request body. */
export const checkResponseErrorBody = <T>(
    responseJson: WuwaAPIResponse<T>,
): responseJson is WuwaAPIServerErrorResponse => {
    return responseJson.code === 500;
};
