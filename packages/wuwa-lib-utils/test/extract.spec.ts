import { describe, it, expect } from "vitest";
import { getPartialConveneHistoryPayloadFromLogFile } from "../src/index.js";

describe("extract", () => {
    it("Should extract the partial convene history payload", async () => {
        const serverId = "mocked server id";
        const playerId = "mocked player id";
        const language = "mocked language";
        const recordId = "mocked record id";
        const resourcesId = "mocked resources id";

        const parameters = new URLSearchParams({
            svr_id: serverId,
            player_id: playerId,
            lang: language,
            record_id: recordId,
            resources_id: resourcesId,
        });

        const mockedLogFileBuffer = Buffer.from(`
            [0607/175600.822:INFO:CONSOLE(15)] "{
                "data": [
                    {
                        "#type": "track",
                        "#time": "2024-06-07 17:56:00.815",
                        "#distinct_id": "XXX",
                        "#event_name": "ta_page_show",
                        "properties": {
                            "#device_id": "XXX",
                            "#zone_offset": 2,
                            "#os": "Windows",
                            "#lib_version": "2.0.0",
                            "#lib": "js",
                            "#screen_height": 1102,
                            "#screen_width": 1658,
                            "#browser": "chrome",
                            "#browser_version": "XXX",
                            "#system_language": "Value exception",
                            "#ua": "XXX",
                            "#utm": "{}",
                            "#url": "https://aki-gm-resources-oversea.aki-game.net/aki/gacha/index.html#/record?${parameters}",
                            "#url_path": "/aki/gacha/index.html",
                            "#title": "《鸣潮》"
                        }
                    }
                ],
                "#app_id": "XXX",
                "#flush_time": 0
            }", source: https://aki-gm-resources-oversea.aki-game.net/aki/gacha/assets/index.js (15)
        `);

        const expected = {
            playerId,
            cardPoolId: resourcesId,
            serverId,
            languageCode: language,
            recordId,
        };

        const actual = getPartialConveneHistoryPayloadFromLogFile(mockedLogFileBuffer);

        await expect(actual).resolves.toEqual(expected);
    });
});
