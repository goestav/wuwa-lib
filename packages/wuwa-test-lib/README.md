# Wuwa Test Lib

> A library with utilities to create testing

## Usage

```ts
import { mockNextFetchResponse, mockedPulls as expected } from "wuwa-test-lib";

it("Should fetch the convene history", async () => {
    mockNextFetchResponse({ code: 0, message: "success", data: mockedPulls });

    const actual = clientWithoutMapping.getConveneHistory({} as ConveneHistoryPayload)

    await expect(actual).resolves.toEqual(expected);
});
```
