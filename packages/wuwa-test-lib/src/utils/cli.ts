const getOptionString = ([optionName, optionValue]: [string, string]): [string, string] => {
    if (optionName.length === 1) return [`-${optionName}`, optionValue];

    return [`--${optionName}`, optionValue];
};

/**
 * Get the option arguments from an object.
 *
 * @example
 * getOptionArgumentsFromObject({ a: 'hello', b: 'world' })     // ['-a hello', '-b world']
 * getOptionArgumentsFromObject({ abc: 'hello', def: 'world' }) // ['--abc hello', '--def world']
 */
export const getOptionArgumentsFromObject = (options: Record<string, string>) =>
    Object.entries(options ?? {}).map((option) => getOptionString(option));
