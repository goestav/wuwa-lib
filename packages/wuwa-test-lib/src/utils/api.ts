import { type MockInstance, vi } from "vitest";
import type { WuwaAPIResponse } from "wuwa-api-types";

/** Mocks a fetch request response. */
export const mockWuwaAPIResponse = (data: WuwaAPIResponse<unknown>, statusCode = 200): Response => {
    return new Response(JSON.stringify(data), { status: statusCode });
};

/**
 * Mocks the next fetch response.\
 * **NOTE:** This will turn {@link fetch|global.fetch} into a {@link https://vitest.dev/api/vi.html#vi-fn|mocked function}.\
 * You can use `(global.fetch as unknown as MockInstance).mockRestore()` to reset to the original implementation.
 *
 * @example
 * it("Should fetch data from the API", async () => {
 *     mockNextFetchResponse({ code: 0, message: "success", data: ['Hello world'] });
 *
 *     await expect(client.fetchData()).resolves.toBe(['Hello world']);
 * });
 */
export const mockNextFetchResponse = (data: WuwaAPIResponse<unknown>, statusCode = 200): void => {
    if (!vi.isMockFunction(global.fetch)) global.fetch = vi.fn();

    const mockedResponse = mockWuwaAPIResponse(data, statusCode);

    (global.fetch as unknown as MockInstance).mockResolvedValueOnce(mockedResponse);
};
