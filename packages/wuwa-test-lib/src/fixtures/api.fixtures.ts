export const mockedPulls = [
    {
        cardPoolType: "6",
        resourceId: 21010023,
        qualityLevel: 3,
        resourceType: "Weapons",
        name: "Originite: Type I",
        count: 1,
        time: "2024-06-01 14:26:54",
    },
    {
        cardPoolType: "6",
        resourceId: 21010023,
        qualityLevel: 3,
        resourceType: "Weapons",
        name: "Originite: Type I",
        count: 1,
        time: "2024-06-01 14:26:53",
    },
    {
        cardPoolType: "6",
        resourceId: 21010023,
        qualityLevel: 3,
        resourceType: "Weapons",
        name: "Originite: Type I",
        count: 1,
        time: "2024-06-01 14:26:52",
    },
];
