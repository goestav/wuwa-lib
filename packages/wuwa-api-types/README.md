# Wuwa API types

> Types for the wuwa API client

## Usage

1. Install the dependency:

    ```sh
    npm install wuwa-api-types
    ```

2. Use it in your project:

    ```ts
    import type { ConveneHistoryPayload } from 'wuwa-api-types'

    const payload: ConveneHistoryPayload = {
        // ...
    }
    ```
