import type { WuwaAPISuccessResponse } from "../api-client/response";
import type { WuwaBanner, WuwaLanguageCode, WuwaServerId } from "../enum";

/** The convene history body payload. */
export interface ConveneHistoryPayload {
    /** The player ID. */
    playerId: string;
    /** The card pool ID. */
    cardPoolId?: string;
    /** The card pool type (aka Convene type/banner). */
    cardPoolType: WuwaBanner;
    /** The server ID. */
    serverId: WuwaServerId | (string & {});
    /**
     * The (presumably) {@link https://en.wikipedia.org/wiki/ISO_639-1|ISO 639-1} language code for the results.
     * @default "en"
     */
    languageCode?: WuwaLanguageCode | (string & {});
    /** The record ID. */
    recordId: string;
}

export interface ConvenePull {
    cardPoolType: string;
    resourceId: number;
    qualityLevel: number;
    resourceType: string;
    name: string;
    count: number;
    time: string;
}

export type ConvenePullHistoryResponse = WuwaAPISuccessResponse<ConvenePull[]>;
