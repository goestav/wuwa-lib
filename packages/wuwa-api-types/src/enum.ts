/** Represents the card pool type (aka Convene type or banner). */
export enum WuwaBanner {
    FeaturedResonator = 1,
    FeaturedWeapon = 2,
    StandardResonator = 3,
    StandardWeapon = 4,
    Beginner = 5,
    /** Beginner's Choice Convene. */
    BeginnersChoice = 6,
    /** Beginner's Choice Convene（Giveback Custom Convene. */
    GivebackCustom = 7,
}

/** Represents the server ID. */
export enum WuwaServerId {
    America = "591d6af3a3090d8ea00d8f86cf6d7501",
    Asia = "86d52186155b148b5c138ceb41be9650",
    Europe = "6eb2a235b30d05efd77bedb5cf60999e",
    /** Hong Kong (HK), Macao (MO), Taiwan (TW). */
    HMT = "919752ae5ea09c1ced910dd668a63ffb",
    /** South East Asia. */
    SEA = "10cd7254d57e58ae560b15d51e34b4c8",
}

/** Represents the language code. */
export enum WuwaLanguageCode {
    English = "en",
    French = "fr",
    German = "de",
    Spanish = "es",
    Korean = "ko",
    Japanese = "ja",
    ChineseSimplified = "zh-Hans",
    ChineseTraditional = "zh-Hant",
}

/** Represents an error code found in the response body of the API. */
export enum WuwaHttpAPIClientErrorCode {
    InternalServerError = -1,
    InvalidServerId = -4,
}
