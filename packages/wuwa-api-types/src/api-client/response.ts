/** Represents a successful API response. */
export interface WuwaAPISuccessResponse<T = unknown> {
    code: number;
    message: string;
    data: T;
}

/** Represents a failed API response. */
export interface WuwaAPIServerErrorResponse {
    code: 500;
    message: string;
}

/** Represents a Wuthering Waves API response. */
export type WuwaAPIResponse<T> = WuwaAPISuccessResponse<T> | WuwaAPIServerErrorResponse;
