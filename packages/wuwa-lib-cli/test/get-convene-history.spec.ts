import { describe, it, expect, vi } from "vitest";
import { createProgram } from "../src/program.js";
import { WuwaAPIClient, WuwaBanner } from "wuwa-api-client";
import type * as WuwaUtils from "wuwa-lib-utils";

describe("get-convene-history", () => {
    it("Should fetch the convene history", async () => {
        const program = createProgram();

        const spy = vi.spyOn(WuwaAPIClient.prototype, "getConveneHistory").mockResolvedValue([]);

        vi.mock(
            "wuwa-lib-utils",
            async (importOriginal) =>
                ({
                    ...(await importOriginal<typeof WuwaUtils>()),
                    getPartialConveneHistoryPayloadFromLogFile: vi.fn().mockResolvedValue({}),
                }) as Partial<typeof WuwaUtils>,
        );

        const banner = WuwaBanner.FeaturedResonator;

        await program.parseAsync(["get-convene-history", "/", "-b", banner.toString()], {
            from: "user",
        });

        expect(spy.mock.lastCall?.[0].cardPoolType).toBe(banner);
    });
});
