import { createProgram } from "./program.js";

const program = createProgram();

program.parse();
