import { Command } from "commander";
import { version } from "../package.json";
import * as commands from "./commands/index.js";

export const createProgram = () => {
    const program = new Command()
        .name("wuwa-lib")
        .description("Command-line interface to interact with the Wuthering Waves API")
        .version(version, "-v, --version", "Output the current version");

    for (const createCommand of Object.values<(program: Command) => Command>(commands)) {
        program.addCommand(createCommand(program));
    }

    return program;
};
