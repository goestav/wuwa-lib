import { Command } from "commander";
import { WuwaBanner, type ConveneHistoryPayload, WuwaAPIClient, WuwaServerId } from "wuwa-api-client";
import { applyDefaults, getPartialConveneHistoryPayloadFromLogFile } from "wuwa-lib-utils";
import { bannerKeys, bannerValues } from "../common.js";
import { commaListAnd, getBufferFromStdin } from "../utils.js";

type ConveneHistoryPartialPayload = Omit<ConveneHistoryPayload, "cardPoolType">;

const createCommand: (program: Command) => Command = (program) => {
    const command = new Command()
        .command("get-convene-history")
        .description("Fetch the convene history")
        .argument("[debug file path]", "The path to the debug file (or manually specify payload options)")
        .requiredOption("-b, --banner <banner name/ID>", "The banner to fetch")
        .option("-p, --player <player id>", "The player ID")
        .option("-c, --card-pool <card pool id>", "The card pool ID")
        .option("-s, --server <server id>", "The server ID")
        .option("-l, --language <language>", "The language to use for the results")
        .option("-r, --record <record id>", "The record ID")
        .action(async (debugFilePath: string | undefined, options) => {
            const client = new WuwaAPIClient({ disableValueMapping: true });
            const pipeBuffer = await getBufferFromStdin();

            const { banner } = options;

            if (!(bannerKeys.includes(banner) || bannerValues.includes(banner)))
                program.error("Banner parameter must be a valid banner name or ID");

            const cardPoolType = (
                bannerValues.includes(banner) ? Number.parseInt(banner) : WuwaBanner[banner]
            ) as WuwaBanner;

            const requiredOptionKeysWithoutDebugFile = ["player", "server", "record"];
            const requiredOptionsWithoutDebugFile = requiredOptionKeysWithoutDebugFile.map((option) => options[option]);

            if (!(debugFilePath || pipeBuffer.length) && !requiredOptionsWithoutDebugFile.every(Boolean)) {
                const missingRequiredOptions = requiredOptionKeysWithoutDebugFile.filter((option) => !options[option]);

                program.error(
                    `The following options are required if a debug file path is not specified: ${commaListAnd(missingRequiredOptions)}`,
                );
            }

            const partialPayload: ConveneHistoryPartialPayload | Record<never, never> =
                debugFilePath || pipeBuffer.length
                    ? await getPartialConveneHistoryPayloadFromLogFile(debugFilePath ?? pipeBuffer)
                    : {};

            const overrideOptions: Partial<ConveneHistoryPayload> = {
                playerId: options.player,
                cardPoolId: options.cardPool,
                serverId: options.server,
                languageCode: options.language,
                recordId: options.record,
            };

            const pullHistory = await client.getConveneHistory({
                ...applyDefaults(partialPayload, { ...overrideOptions, cardPoolType }),
                cardPoolType,
            } as ConveneHistoryPayload);

            console.log(JSON.stringify(pullHistory));
        });

    const programName = program.name();
    const commandName = command.name();

    command.addHelpText(
        "afterAll",
        `

Examples:
  $ ${programName} ${commandName} --banner ${bannerValues[0]} /path/to/debug.log
  $ ${programName} ${commandName} --banner ${bannerKeys[0]} /path/to/debug.log
  $ cat /path/to/debug.log | ${programName} ${commandName} --banner ${bannerKeys[2]}
  $ ${programName} ${commandName} --banner ${bannerKeys[0]} --player-id 12345679 --server-id ${WuwaServerId.Europe} --record-id de4670a21b5e93b795d5bdf633c1e7b6 --json`,
    );

    return command;
};

export { createCommand as createConveneHistoryCommand };
