import { Command } from "commander";
import { getPartialConveneHistoryPayloadFromLogFile } from "wuwa-lib-utils";
import { getBufferFromStdin } from "../utils.js";

const createCommand: (program: Command) => Command = (program) => {
    const command = new Command("get-partial-payload")
        .argument("[debug file path]", "The path to the debug file")
        .description("Extract the partial payload from a debug.log file")
        .action(async (debugFilePath: string) => {
            const pipeBuffer = await getBufferFromStdin();

            if (!(debugFilePath || pipeBuffer.length)) program.error("A debug file path is required");

            const partialPayload = await getPartialConveneHistoryPayloadFromLogFile(
                pipeBuffer.length ? pipeBuffer : debugFilePath,
            );

            console.log(JSON.stringify(partialPayload));
        });

    const programName = program.name();
    const commandName = command.name();

    command.addHelpText(
        "afterAll",
        `
    
Examples:
  $ ${programName} ${commandName} /path/to/debug.log
  $ cat /path/to/debug.log | ${programName} ${commandName}}`,
    );

    return command;
};

export { createCommand as createGetPartialPayloadCommand };
