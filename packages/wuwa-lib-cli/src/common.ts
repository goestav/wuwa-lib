import { WuwaBanner } from "wuwa-api-client";
// import { createProgram } from "./program.js";

export const bannerKeys = Object.keys(WuwaBanner).filter((value) => Number.isNaN(Number(value)));
export const bannerValues = Object.keys(WuwaBanner).filter((value) => !Number.isNaN(Number(value)));

// export const program = createProgram();
