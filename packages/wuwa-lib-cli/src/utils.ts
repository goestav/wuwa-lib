/**
 * Create a comma separated list with "and" for the last element.
 *
 * @example
 * commaListAnd([1])       // '1'
 * commaListAnd([1, 2])    // '1 and 2'
 * commaListAnd([1, 2, 3]) // '1, 2 and 3'
 */
export const commaListAnd = (input: unknown[]) => {
    if (input.length <= 1) return input[0]?.toString() ?? "";

    const beforeAndList = input.slice(0, -1);
    const afterAndList = input.slice(-1);

    const beforeAnd = beforeAndList.join(", ");
    const afterAnd = afterAndList.join(", ");

    return `${beforeAnd} and ${afterAnd}`;
};

/** Creates a {@link Buffer} from the {@link process.stdin}. */
export const getBufferFromStdin = () =>
    new Promise<Buffer>((resolve, reject) => {
        if (process.stdin.isTTY) {
            resolve(Buffer.alloc(0));
            return;
        }

        const data: Buffer[] = [];

        process.stdin.on("data", (chunk) => data.push(chunk));

        process.stdin.on("end", () => resolve(Buffer.concat(data)));

        process.stdin.on("error", (error) => reject(error));
    });
