# Wuwa Lib CLI

> Command-line interface to interact with the Wuthering Waves API

## Usage

1. Install the dependency:

    ```sh
    npm install --global wuwa-lib-cli
    ```

2. Get the help page for the CLI:

    ```sh
    wuwa-lib help
    ```
