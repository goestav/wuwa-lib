import { describe, it, expect } from "vitest";
import { WuwaServerId, type ConveneHistoryPayload } from "wuwa-api-types";
import { WuwaAPIClient } from "../src/client/index.ts";
import { HttpAPIClientError } from "../src/http/errors.ts";
import { mockNextFetchResponse, mockedPulls } from "wuwa-test-lib";
import { getUTCTimeZoneOffsetForServer } from "wuwa-lib-utils";

const client = new WuwaAPIClient();
const clientWithoutMapping = new WuwaAPIClient({ disableValueMapping: true });

describe("getConveneHistory", () => {
    it("Should fetch the convene history", async () => {
        mockNextFetchResponse({ code: 0, message: "success", data: mockedPulls });

        await expect(clientWithoutMapping.getConveneHistory({} as ConveneHistoryPayload)).resolves.toEqual(mockedPulls);
    });

    it("Should fetch the convene history with mapped value types", async () => {
        const mockedPullsWithDates = mockedPulls.map((pull) => ({ ...pull, time: new Date(`${pull.time}Z`) }));

        mockNextFetchResponse({ code: 0, message: "success", data: mockedPulls });

        await expect(client.getConveneHistory({} as ConveneHistoryPayload)).resolves.toEqual(mockedPullsWithDates);

        // Test time offset for a wuthering waves server

        const serverId = WuwaServerId.Europe;
        const serverTimeZoneOffset = getUTCTimeZoneOffsetForServer(serverId);
        const mockedPullsWithEUServerDates = mockedPulls.map((pull) => ({
            ...pull,
            time: new Date(`${pull.time}Z${serverTimeZoneOffset}`),
        }));

        mockNextFetchResponse({ code: 0, message: "success", data: mockedPulls });

        const actual = client.getConveneHistory({ serverId } as ConveneHistoryPayload);

        await expect(actual).resolves.toEqual(mockedPullsWithEUServerDates);
    });

    it("Should throw an error when the status code is not 2XX", async () => {
        mockNextFetchResponse({ code: 500, message: "some error" }, 500);

        await expect(client.getConveneHistory({} as ConveneHistoryPayload)).rejects.toThrowError(HttpAPIClientError);
    });

    it("Should throw an error when the status inside the response body indicates an error", async () => {
        mockNextFetchResponse({ code: -1, message: "some error", data: [] });

        await expect(client.getConveneHistory({} as ConveneHistoryPayload)).rejects.toThrowError(HttpAPIClientError);
    });
});
