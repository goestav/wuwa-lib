# Wuwa API Client

> An API client for Wuthering Waves

## Usage

1. Install the dependency:

    ```sh
    npm install wuwa-api-client
    ```

2. Use it in your project:

    ```js
    import { WuwaAPIClient } from 'wuwa-api-client'

    const client = new WuwaAPIClient();

    const conveneHistory = await client.getConveneHistory({
        // ...
    });

    console.log(conveneHistory)
    ```
