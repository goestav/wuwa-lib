import { HttpAPIClientError } from "./errors.js";

export interface HttpAPIClientRequestInit extends RequestInit {
    // biome-ignore lint/suspicious/noExplicitAny: body gets transformed automatically into the right type
    body?: any;
}

type PreRequestHook = (request: Request) => Promise<void> | void;
type PostRequestHook<T = unknown> = (request: Request, response: Response) => Promise<T> | T;

export interface HttpAPIClientOptions {
    /** The default {@link HttpAPIClientRequestInit} options to fallback to. */
    defaultRequestInit?: HttpAPIClientRequestInit;
    /** Hook that runs before making the {@link fetch} request. */
    preRequestHook?: PreRequestHook;
    /**
     * Hook that runs after making the {@link fetch} request.\
     * This hook should return the body.
     */
    postRequestHook?: PostRequestHook;
}

/** Represents a HTTP API client. */
export class HttpAPIClient {
    constructor(
        public readonly baseUrl: string,
        public readonly options?: HttpAPIClientOptions,
    ) {}

    /**
     * Make an HTTP request.
     *
     * @throws {HttpAPIClientError}
     */
    public async request<T = unknown>(endpoint: string, options?: HttpAPIClientRequestInit) {
        const url = new URL(endpoint, this.baseUrl);

        const request = new Request(url, {
            ...this.options?.defaultRequestInit,
            ...options,
            body: this.transformBody(options?.body),
            headers: this.applyDefaultHeaders(this.options?.defaultRequestInit?.headers, options?.headers),
        });

        await this.options?.preRequestHook?.(request);

        const response = await fetch(request.clone());

        if (!response.ok)
            throw new HttpAPIClientError(response.statusText, {
                requestInstance: request,
                responseInstance: response.clone(),
            });

        if (this.options?.postRequestHook) return this.options?.postRequestHook?.(request, response) as T;

        return response.json() as T;
    }

    public async post<T>(endpoint: string, options?: HttpAPIClientRequestInit) {
        return this.request<T>(endpoint, { ...options, method: "POST" });
    }

    public async get<T>(endpoint: string, options?: HttpAPIClientRequestInit) {
        return this.request<T>(endpoint, { ...options, method: "GET" });
    }

    public async patch<T>(endpoint: string, options?: HttpAPIClientRequestInit) {
        return this.request<T>(endpoint, { ...options, method: "PATCH" });
    }

    public async put<T>(endpoint: string, options?: HttpAPIClientRequestInit) {
        return this.request<T>(endpoint, { ...options, method: "PUT" });
    }

    public async delete<T>(endpoint: string, options?: HttpAPIClientRequestInit) {
        return this.request<T>(endpoint, { ...options, method: "DELETE" });
    }

    /** Transform the body into the correct type. */
    protected transformBody(body?: HttpAPIClientRequestInit["body"]) {
        if (!body) return body;

        if (typeof body === "object") return JSON.stringify(body);

        return body;
    }

    /** Merge the default and current headers. */
    protected applyDefaultHeaders(defaultHeadersInit?: HeadersInit, currentHeadersInit?: HeadersInit) {
        const defaultHeaders = new Headers(defaultHeadersInit);
        const currentHeaders = new Headers(currentHeadersInit);

        for (const [headerName, defaultHeaderValue] of defaultHeaders) {
            const currentHeaderValue = currentHeaders.get(headerName);

            currentHeaders.set(headerName, currentHeaderValue ?? defaultHeaderValue);
        }

        return currentHeaders;
    }
}
