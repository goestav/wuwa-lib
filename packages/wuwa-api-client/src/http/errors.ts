export interface HttpAPIClientErrorOptions extends ErrorOptions {
    /** The {@link Response|fetch Request instance}. */
    requestInstance: Request;
    /** The {@link Response|fetch Response instance}. */
    responseInstance: Response;
}

/** Represents an HTTP API client error. */
export class HttpAPIClientError extends Error {
    private _options: HttpAPIClientErrorOptions;

    constructor(message: string, options: HttpAPIClientErrorOptions) {
        super(message, options);

        this._options = options;
    }

    /** The {@link Response|response} for this error. */
    public get response() {
        return this._options.responseInstance;
    }
}
