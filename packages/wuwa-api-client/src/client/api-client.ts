import { checkResponseSuccessBody } from "wuwa-lib-utils";
import { HttpAPIClient, HttpAPIClientError } from "../http/index.js";
import * as endpoints from "./endpoints/index.js";

export interface WuwaAPIClientOptions {
    /** Disables mapping values to javascript types (e.g: date strings to javascript dates). */
    disableValueMapping?: boolean;
}

/** Extract the options generic from the {@link WuwaAPIClient}. */
export type GetClientOptions<Client extends WuwaAPIClient> = Client extends WuwaAPIClient<infer T> ? T : never;

/** Extract the value of a generic option of the {@link WuwaAPIClient}. */
export type GetClientOption<
    Client extends WuwaAPIClient,
    Option extends keyof GetClientOptions<Client>,
> = GetClientOptions<Client>[Option];

/** Represents the Wuthering Waves API Client. */
export class WuwaAPIClient<Options extends WuwaAPIClientOptions = WuwaAPIClientOptions> {
    protected httpClient = new HttpAPIClient("https://gmserver-api.aki-game2.net", {
        defaultRequestInit: {
            headers: {
                "Content-Type": "application/json",
            },
        },
        async postRequestHook(request, response) {
            const data = await response.json();

            // Raise error when status inside body indicates an error
            if (!checkResponseSuccessBody(data))
                throw new HttpAPIClientError(data.message, {
                    requestInstance: request,
                    responseInstance: response,
                });

            return data;
        },
    });

    constructor(public readonly options?: Options) {}
}

// Load endpoint methods + types in

type Endpoints = typeof endpoints;

for (const [endpointName, endpointMethod] of Object.entries(endpoints)) {
    WuwaAPIClient.prototype[endpointName as keyof Endpoints] = endpointMethod;
}

declare module "./index.js" {
    interface WuwaAPIClient extends Endpoints {}
}
