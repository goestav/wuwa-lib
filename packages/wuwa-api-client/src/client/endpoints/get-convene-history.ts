import type { ConveneHistoryPayload, ConvenePull, WuwaAPISuccessResponse } from "wuwa-api-types";
import type { GetClientOption, WuwaAPIClient } from "../api-client.js";
import { getDateFromString } from "wuwa-lib-utils";

type MappedConvenePull = Omit<ConvenePull, "time"> & { time: Date };

/** Fetch the convene history. */
export async function getConveneHistory(this: WuwaAPIClient, payload: ConveneHistoryPayload) {
    const { data: convenePulls } = await this.httpClient.post<WuwaAPISuccessResponse<ConvenePull[]>>(
        "/gacha/record/query",
        {
            body: { ...payload, languageCode: payload.languageCode ?? "en" } as ConveneHistoryPayload,
        },
    );

    const mappedConvenePulls = this.options?.disableValueMapping
        ? convenePulls
        : convenePulls.map((pull) => ({
              ...pull,
              time: getDateFromString(pull.time, payload.serverId),
          }));

    return mappedConvenePulls as GetClientOption<typeof this, "disableValueMapping"> extends true
        ? ConvenePull[]
        : MappedConvenePull[];
}
