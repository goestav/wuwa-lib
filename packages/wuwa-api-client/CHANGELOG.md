# wuwa-api-client

## 1.0.0

### Major Changes

- e0dbeb2: First release

### Patch Changes

- Updated dependencies [e0dbeb2]
  - wuwa-api-types@1.0.0
  - wuwa-lib-utils@1.0.0
