/** @type {typeof import('@changesets/cli/commit').default} */
module.exports = {
    getVersionMessage: (changeset) => changeset.preState?.tag
        ? `chore: version packages (${changeset.preState.tag})`
        : `chore: version packages`,
}
