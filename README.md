# Wuwa Lib

> A library for Wuthering Waves

## Features

- Full TypeScript support
- [CommonJS](https://en.wikipedia.org/wiki/CommonJS) and [ESM](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules) support
- No external dependencies (except [commander](https://www.npmjs.com/package/commander) for the CLI)

## Development

1. If it's your first time setting up the project it's recommended to build all packages sequentially first:

    ```sh
    pnpm build:sequential
    ```

    You can start using the `build` or `watch` scripts after this

2. Make your changes and document them with `pnpm changeset`

3. Commit your changes and push them to the

4. Submit a merge/pull request

### Packages

| Package                                                   | Description                                               |
| --------------------------------------------------------- | --------------------------------------------------------- |
| [`wuwa-api-client`](./packages/wuwa-api-client/README.md) | An API client for Wuthering Waves                         |
| [`wuwa-api-types`](./packages/wuwa-api-types/README.md)   | The types for the API (exported by `wuwa-api-client` too) |
| [`wuwa-lib-cli`](./packages/wuwa-lib-cli/README.md)       | A command-line interface to interact with the API         |
| [`wuwa-lib-utils`](./packages/wuwa-lib-utils/README.md)   | Useful utilities related to the API                       |
